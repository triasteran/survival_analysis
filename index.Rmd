---
title: "Test GitLab pages book for Survival hints in R"
date: "`r Sys.Date()`"
site: bookdown::bookdown_site
documentclass: book
output:
  bookdown::gitbook: 
  bookdown::pdf_book: 
    latex_engine: pdflatex
---


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

# Libraries

```{r}
library(ggplot2)  
library(survminer)
library(survival)
library(ggpubr)
library(dplyr)
library(reshape2)
```

# Intro

Survival time = time to event:
* relapse
* progression
* death

The event may not be observed for some individuals within the study time period, producing the so-called censored observations.

Survival analysis can handle right censoring. 

* Survival probability S(t) is the probability that an individual survives from the time origin (e.g. diagnosis of cancer) to a specified future time t

* Hazard h(t) is the probability that an individual who is under observation at a time t has an event at that time

# Kaplan-Meier plots to visualize survival curves

The Kaplan-Meier (KM) method is a non-parametric method used to estimate the survival probability from observed survival times.

It uses the assumption that the probability of surviving past a certain time point t is equal to the product of the observed survival rates until time point t. 

The survival probability at time t is given by S(t) = p.1 * p.2 * … * p.t: 
* p.1 being the proportion of all patients surviving past the first time point
* p.2 being the proportion of patients surviving past the second time point
* ...
* and so forth until time point t is reached

Censored patients are omitted after the time point of censoring, so they do not influence the proportion of surviving patients. 

Data 'lung'.

Main columns:
* time: Survival time in days
* status: censoring status 1=censored, 2=dead
* sex: Male=1 Female=2

Usually survival data looks like:
* time patients were tracked until they either died or were lost to follow-up
* censor: died (1) or lost (0), can be also 2 (dead) and 1 (censored) as in this case

## Survfit function

```{r}
data("lung")
head(lung, n=3)

fit <- survfit(Surv(time, status) ~ sex, data = lung)
print(fit)

# Summary of survival curves
head(summary(fit), n=2)

# Access to the sort summary table
summary(fit)$table

d <- data.frame(time = fit$time,
                  n.risk = fit$n.risk,
                  n.event = fit$n.event,
                  n.censor = fit$n.censor,
                  surv = fit$surv,
                  upper = fit$upper,
                  lower = fit$lower
                  )
head(d, n=3)
```

## Visualizing Kaplan-Meier survival curve

```{r fig.height=8.5, fig.width=6}
ggsurvplot(
   fit,                     # survfit object with calculated statistics.
   pval = TRUE,             # show p-value of log-rank test.
   conf.int = TRUE,         # show confidence intervals for 
                            # point estimaes of survival curves.
   conf.int.style = "step",  # customize style of confidence intervals
   xlab = "Time in days",   # customize X axis label.
   break.time.by = 200,     # break X axis in time intervals by 200.
   ggtheme = theme_light(), # customize plot and risk table with a theme.
   risk.table = "abs_pct",  # absolute number and percentage at risk.
  risk.table.y.text.col = T,# colour risk table text annotations.
  risk.table.y.text = FALSE,# show bars instead of names in text annotations
                            # in legend of risk table.
  ncensor.plot = TRUE,      # plot the number of censored subjects at time t
  surv.median.line = "hv",  # add the median survival pointer.
  legend.labs = 
    c("Male", "Female"),    # change legend labels.
  palette = 
    c("darkgoldenrod", "blueviolet") # custom color palettes.
)
```


```{r}
ggsurvplot(fit,
          conf.int = TRUE,
          risk.table.col = "strata", # Change risk table color by groups
          ggtheme = theme_bw(), # Change ggplot2 theme
          palette = c("darkgoldenrod", "blueviolet"),
          xlim = c(0, 600))
```

## Cummulative hazard  

Cummulative hazard  is commonly used to estimate the hazard probability: H(t) = -log(S(t)).

H(t) - the number of events that would be expected for each individual by time t if the event were a repeatable process.

Cumulative hazard curve:

```{r}
ggsurvplot(fit,
          conf.int = TRUE,
          risk.table.col = "strata", # Change risk table color by groups
          ggtheme = theme_bw(), # Change ggplot2 theme
          palette = c("darkgoldenrod", "blueviolet"),
          fun = 'cumhaz')
```

Cumulative events curve, f(y) = 1-y, fun = 'event'

Log transformation of the survivor function: fun = "log"

## Log-rank test to compare the survival curves of two or more groups

Non-parametric test, which makes no assumptions about the survival distributions.

* H0:  there is no difference in survival between the two groups (survival curves were identical)
* H1: there is a difference in survival between the two groups

It compares the observed number of events in each group to what would be expected if the null hypothesis were true.

The log rank statistic is approximately distributed as a chi-square test statistic.

```{r}
surv_diff <- survdiff(Surv(time, status) ~ sex, data = lung)
surv_diff
```

## Survival curves based on multiple factors

```{r}
fit2 <- survfit(Surv(time, status) ~ sex + rx + adhere,data = colon )

# Plot survival curves by sex and facet by rx and adhere
ggsurv <- ggsurvplot(fit2, 
                     conf.int = TRUE,
                     ggtheme = theme_bw())
   
ggsurv$plot +theme_bw() + 
  theme (legend.position = "right")+
  facet_grid(rx ~ adhere)
```


##  Multiple pairwise comparisons of survival curves

```{r fig.height=4, fig.width=6}
data(myeloma)

fit3 <- survfit(Surv(time, event) ~ molecular_group, data = myeloma)

ggsurvplot(fit3, 
           pval = T,
           conf.int = F,
           ggtheme = theme_bw())

# Pairwise survdiff
res <- pairwise_survdiff(Surv(time, event) ~ molecular_group,
     data = myeloma)
res
```


# Cox proportional hazards regression to describe the effect of variables on survival

The purpose of the model is to evaluate simultaneously the effect of several factors on survival. 

It allows us to examine how specified factors influence the rate of a particular event happening (e.g., infection, death) at a particular point in time. 

* It does not assume an underlying probability distribution 
* It assumes that the hazards of the patient groups you compare are constant over time! (the hazard curves for the groups of observations (or patients) should be proportional and cannot cross)

Hazard function can be interpreted as the risk of dying at time t:

h(t) = h0(t) * exp(b1*x1 + b2*x2 + ... +bp*xp)
* x1, x2, ... = covariates
* b1, b2, ... = coefficients (weights)
* h0 = baseline hazard

HR (Hazard Ration) = quantities exp(bi)

* HR = 1: No effect
* HR < 1: Reduction in the hazard
* HR > 1: Increase in Hazard

If an individual has a risk of death at some initial time point that is twice as high as that of another individual, then at all later times the risk of death remains twice as high (the HR for 2 patients is independent of the time t). 

## Statistical test for one variable

*  z = Wald statistic value, z = coef/se(coef), it shows whether the beta (β) coefficient of a given variable is statistically significantly different from 0 (-3.176)

* regression coefficients: A positive sign means that the hazard (risk of death) is higher, and thus the prognosis worse, for subjects with higher values of that variable (-0.5310)

Here is comparison: the second group relative to the first group, so the secod group has better prognosis (lower risk of death) than first group. 

* Hazard ratios = exp(coef), (0.5880)

* Confidence intervals of the hazard ratios (0.4237-0.816)

* Global statistical significance of the model: p-values for three alternative tests for overall significance of the model: The likelihood-ratio test, Wald test, and score logrank statistics (p=0.001)


```{r}
res.cox <- coxph(Surv(time, status) ~ sex, data = lung)
res.cox
summary(res.cox)
```

## Statistical test for multiple covariates 

```{r}
covariates <- c("age", "sex",  "ph.karno", "ph.ecog", "wt.loss")
univ_formulas <- sapply(covariates,
                        function(x) as.formula(paste('Surv(time, status)~', x)))

univ_models <- lapply(univ_formulas, function(x){coxph(x, data = lung)})
# Extract data 
univ_results <- lapply(univ_models,
                       function(x){ 
                          x <- summary(x)
                          p.value<-signif(x$wald["pvalue"], digits=2)
                          wald.test<-signif(x$wald["test"], digits=2)
                          beta<-signif(x$coef[1], digits=2);#coeficient beta
                          HR <-signif(x$coef[2], digits=2);#exp(beta)
                          HR.confint.lower <- signif(x$conf.int[,"lower .95"], 2)
                          HR.confint.upper <- signif(x$conf.int[,"upper .95"],2)
                          HR <- paste0(HR, " (", 
                                       HR.confint.lower, "-", HR.confint.upper, ")")
                          res<-c(beta, HR, wald.test, p.value)
                          names(res)<-c("beta", "HR (95% CI for HR)", "wald.test", 
                                        "p.value")
                          return(res)
                          #return(exp(cbind(coef(x),confint(x))))
                         })
res <- t(as.data.frame(univ_results, check.names = FALSE))
as.data.frame(res)


```

Skip non-significant covariates:

```{r}
res.cox <- coxph(Surv(time, status) ~ age + sex + ph.ecog, data =  lung)
summary(res.cox)
```
## Forest-plot

Every HR represents a relative risk of death that compares one instance of a binary feature to the other instance.

For example, a hazard ratio of 0.25 for treatment groups tells you that patients who received treatment B have a reduced risk of dying compared to patients who received treatment A (which served as a reference to calculate the hazard ratio).

```{r}
ggforest(res.cox, data = lung)
```
Ovarian dataset
Change variables and plot forest:

```{r}
# Dichotomize age and change data labels
ovarian$rx <- factor(ovarian$rx, 
                     levels = c("1", "2"), 
                     labels = c("A", "B"))
ovarian$resid.ds <- factor(ovarian$resid.ds, 
                         
                             levels = c("1", "2"), 
                           labels = c("no", "yes"))
ovarian$ecog.ps <- factor(ovarian$ecog.ps, 
                          levels = c("1", "2"), 
                          labels = c("good", "bad"))
ovarian <- ovarian %>% mutate(age_group = ifelse(age >=50, "old", "young"))
ovarian$age_group <- factor(ovarian$age_group)

surv_object <- Surv(time = ovarian$futime, event = ovarian$fustat)

fit.coxph <- coxph(surv_object ~ rx + resid.ds + age_group + ecog.ps, 
                   data = ovarian)
ggforest(fit.coxph, data = ovarian)
  
```


## Cox proportional hazards model diagnostics 

* Schoenfeld residuals to check the proportional hazards assumption
* Martingale residual to assess nonlinearity
* Deviance residual (symmetric transformation of the Martinguale residuals), to examine influential observations

### Testing proportional hazard assumption: the Schoenfeld residuals are independent of time

```{r}
res.cox <- coxph(Surv(time, status) ~ age + sex + wt.loss, data =  lung)
res.cox
```

Test is not significant => we can assume the proportional hazards

```{r}
test.ph <- cox.zph(res.cox)
test.ph
```

Graphical diagnostic (the solid line is a smoothing spline fit to the plot, with the dashed lines representing a +/- 2-standard-error band around the fit): 

```{r fig.height=8, fig.width=6}
ggcoxzph(test.ph)
```

### Testing influential observations (outliers): the deviance residuals or the dfbeta values

dfbeta: plots the estimated changes in the regression coefficients upon deleting each observation in turn

dfbetas: estimated changes in the coefficients divided by their standard errors

Here: none of the observations is terribly influential individually, even though some of the dfbeta values for age and wt.loss are large compared with the others.

```{r fig.height=4, fig.width=9}
ggcoxdiagnostics(res.cox, type = "dfbeta",
                 linear.predictions = FALSE, ggtheme = theme_bw())
```
check outliers by deviance residuals (normalized transform of the martingale residual. These residuals should be roughtly symmetrically distributed about zero with a standard deviation of 1):

```{r}
ggcoxdiagnostics(res.cox, type = "deviance",
                 linear.predictions = FALSE, ggtheme = theme_bw())
```

### Non-linearity model testing 

Plotting the Martingale residuals against continuous covariates.

Here: nonlinearity is slightly expressed 

```{r  fig.height=9, fig.width=4}
ggcoxfunctional(Surv(time, status) ~ age + log(age) + sqrt(age), data = lung)
```








# Survival data can be approximated by Weibull model distribution

data GBSG2 contains the observations of 686 women from Breast Cancer Study

Important columns:

* time: recurrence free survival time (in days).
* cens: censoring indicator (0- censored, 1- event)
* horTh: hormonal therapy, a factor at two levels no and yes
* tsize: tumor size (in mm)


```{r}
data('GBSG2', package = 'TH.data')
head(GBSG2)
table(GBSG2$cens)

wb <- survreg(Surv(time, cens) ~ 1, data = GBSG2)
summary(wb)
```

E.g. 90% of all patiants survive beyond time point - 384.9947: 
(dist function = 1 - survival function)

```{r}
predict(wb, type = 'quantile', p = 1-0.9, newdata = data.frame(1))
```

* make vector of quantiles (probabilities)
* predict times for each probability using weibull model built from the data 

```{r}
surv <- seq(.99, .01, by = -.01) 

t <- predict(wb, type = "quantile", p = 1 - surv, newdata = data.frame(1))

head(data.frame(time = t, surv = surv), n=3)
```
* plot survival curve

